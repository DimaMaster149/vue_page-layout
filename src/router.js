import Vue from 'vue'
import Router from 'vue-router'
import MainPage from './views/MainPage.vue'
import Dashboard from './views/Dashboard.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
     {
          path: '/dashboard',
          name: 'Dashboard',
          component: Dashboard
      },
  ]
})
