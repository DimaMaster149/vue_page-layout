import Datepicker from 'vuejs-datepicker';
export default {
  data: function () {
  return {
    activetab: 1
  }
},
  methods: {
     inputChange: function(){
    if(document.getElementById('searchBox').value !== '')
    {
      document.getElementById('searchPlaceholder').style.display = 'none';
    }
    else{
      document.getElementById('searchPlaceholder').style.display = 'block';
    }
  },
  openFirstPicker: function() {
      this.$refs.firstPicker.showCalendar();
    },
    openSecondPicker: function() {
        this.$refs.secondPicker.showCalendar();
      },
},
components: {
  Datepicker
}
}
